package uk.co.hcssoftware;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class ShoppingCartTest {
	
	private ShoppingCart shoppingCart = new ShoppingCart();
	
	@Test
	public void canCalculateCostOfSingleApple() {
		final String total = shoppingCart.calculateTotalCost(asList("Apple"));
		
		assertThat(total, is("£0.60"));
	}
	
	@Test
	public void canCalculateCostOfMultipleApples() {
	    final String total = shoppingCart.calculateTotalCost(asList("Apple", "Apple", "Apple"));
	    
	    assertThat(total, is("£1.20"));
	}
	
	@Test
	public void canCalculateCostOfSingleOrange() {
		final String total = shoppingCart.calculateTotalCost(asList("Orange"));
		
		assertThat(total, is("£0.25"));
	}
	
	@Test
	public void canCalculateCostOfMultipleOranges() {
	    final String total = shoppingCart.calculateTotalCost(asList("Orange", "Orange", "Orange", "Orange"));
	    
	    assertThat(total, is("£0.75"));
	}
	
	@Test
	public void canCalculateMixtureOfApplesAndOranges() {
		final String total = shoppingCart.calculateTotalCost(asList("Apple", "Apple", "Orange", "Apple", "Orange"));
		
		assertThat(total, is("£1.70"));
	}
	
	@Test
	public void returnZeroWhenNoItems() {
		final String total = shoppingCart.calculateTotalCost(asList());
		
		assertThat(total, is("£0.00"));
	}
	
	@Test
	public void canCalculateBuyOneGetOneFreeOnApples() {
		final String total = shoppingCart.calculateTotalCost(asList("Apple", "Apple"));
	    
	    assertThat(total, is("£0.60"));
	}
	
	@Test
	public void canCalculate3For2OnOranges() {
		final String total = shoppingCart.calculateTotalCost(asList("Orange", "Orange", "Orange"));
		
		assertThat(total, is("£0.50"));
	}
	
}