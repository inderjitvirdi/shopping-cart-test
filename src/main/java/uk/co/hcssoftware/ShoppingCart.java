package uk.co.hcssoftware;

import static java.math.RoundingMode.HALF_UP;

import java.math.BigDecimal;
import java.util.List;

public class ShoppingCart {

	private static final double ORANGE_COST = 0.25;
	private static final double APPLE_COST = 0.6;
	private static final String ORANGE = "Orange";
	private static final String APPLE = "Apple";

	public String calculateTotalCost(final List<String> items) {
		final long numApples = countOccurrances(items, APPLE);
		final long numOranges = countOccurrances(items, ORANGE);
		
		double cost = calculateAppleCost(numApples) + calculateOrangesCost(numOranges);
		
		return format(cost);
	}

	private double calculateOrangesCost(final long numOranges) {
		return (numOranges * ORANGE_COST) - ((numOranges / 3) * ORANGE_COST);
	}

	private double calculateAppleCost(final long numApples) {
		return ((numApples / 2) + (numApples % 2)) * APPLE_COST;
	}

	private long countOccurrances(final List<String> items, final String itemToCount) {
		return items.stream().filter(item -> item.equals(itemToCount)).count();
	}

	private String format(final double cost) {
		return "£" + new BigDecimal(cost).setScale(2, HALF_UP).toPlainString();
	}
	
}